<?php
/* enqueue script for parent theme stylesheeet */
function bfx_childtheme_parent_styles()
{

    // enqueue style
    wp_enqueue_style('parent', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'bfx_childtheme_parent_styles');


if (!defined('BFX_NUDE_THEME_MENU_NUMBERS')) {
    define('BFX_NUDE_THEME_MENU_NUMBERS', 3);
}

function bfx_custom_new_menu()
{
    for ($i = 1; $i < BFX_NUDE_THEME_MENU_NUMBERS + 1; $i++) {
        register_nav_menu('menu-' . $i, __('Menu ' . $i));
    }
}
add_action('init', 'bfx_custom_new_menu');

function bfx_init_theme_supports()
{
    add_theme_support('post-thumbnails');
    add_theme_support('align-wide');
    add_post_type_support( 'page', 'excerpt' );
}

add_action('after_setup_theme', 'bfx_init_theme_supports');

function bfx_remove_default_category_from_permalink($permalink, $post, $leavename)
{
    $permalink_structure = get_option('permalink_structure');
    if (in_array('category', get_post_taxonomies($post)) && strpos($permalink_structure, '%category%/') !== false) {

        // $post_belongs_to_default_category = false;
        // $post_default_category = null;
        $string_to_be_replaced_from_permalink = '';
        $default_category_id = get_option('default_category');
        $post_cats = get_the_category($post->ID);

        if (!empty($default_category_id)) {
            // if post doesn't belong to any category, so it should belong to the default category
            // The default category assigned if no category has been chosen doesn't vary between 
            // languages in Polylang. Instead, in order to switch the language for the default category
            // you should choose one of the translated default categories.
            if (empty($post_cats)) {
                // So this is the case where the untranslated default category appears in pemalink, even 
                // if the post is translated
                $default_category = get_category($default_category_id);
                $string_to_be_replaced_from_permalink = $default_category->slug;
            } else {
                // This is the case where the post may have a translated default category, so we
                // have to remove it from the permalink.
                // Now we check if post has default category.
                // First of all we have to get all the default categories.
                // We should support Polylang, so there could be translated default categories
                if (function_exists('pll_languages_list') && function_exists('pll_get_term')) {
                    $default_categories_ids_list = [];
                    $languages_list = pll_languages_list();
                    foreach ($languages_list as $a_language) {
                        $result = pll_get_term($default_category_id, $a_language);
                        if ($result) {
                            $default_categories_ids_list[$a_language] = $result;
                        }
                    }
                } else {
                    $default_categories_ids_list[] = $default_category_id;
                }

                // Now that we have all the default categories, let's check if the post
                // belongs to one of them
                foreach ($post_cats as $a_cat) {

                    // $post_belongs_to_default_category = true;
                    // But now we have to find which one. If we have Polylang installed, the category
                    // id changes on the basis of the post language
                    if (function_exists('pll_get_post_language')) {
                        $post_language = pll_get_post_language($post->ID);
                        if (isset($default_categories_ids_list[$post_language])) {
                            $category = get_category($default_categories_ids_list[$post_language]);
                            $string_to_be_replaced_from_permalink = $category->slug;
                        }
                    } else {
                        if (in_array($a_cat->term_id, $default_categories_ids_list)) {
                            $key = array_search($a_cat->term_id, $default_categories_ids_list);
                            $category = get_category(($default_categories_ids_list[$key]));
                            $string_to_be_replaced_from_permalink = $category->slug;
                        }
                    }
                }
            }
        }

        if ($string_to_be_replaced_from_permalink) {
            $string_to_be_replaced_from_permalink = '/' . $string_to_be_replaced_from_permalink . '/';
            $permalink = str_replace($string_to_be_replaced_from_permalink, '/', $permalink);
        }
    }
    return $permalink;
}

add_filter('post_link', 'bfx_remove_default_category_from_permalink', 9, 3);
add_filter('post_type_link', 'bfx_remove_default_category_from_permalink', 9, 3);

function bfx_remove_default_category_from_yoast_breadcrumbs($links)
{
    $default_category_id = get_option('default_category');
    $default_category_ids = [];
    // Polylang support
    if (function_exists('pll_languages_list') && function_exists('pll_get_term')) {
        $languages_list = pll_languages_list();
        foreach ($languages_list as $a_language) {
            $result = pll_get_term($default_category_id, $a_language);
            if ($result) {
                $default_category_ids[] = $result;
            }
        }
    } else {
        $default_category_ids = [$default_category_id];
    }
    $filtered_links = [];
    foreach ($links as $a_link) {
        if (isset($a_link['term_id']) && !in_array($a_link['term_id'], $default_category_ids)) {
            $filtered_links[] = $a_link;
        }
    }

    return $filtered_links;
}

add_filter("wpseo_breadcrumb_links", "bfx_remove_default_category_from_yoast_breadcrumbs");

// if (function_exists('acf_add_options_page')) {
//     acf_add_options_page([
//         'page_title' => __('Site settings', 'theme'),
//         'menu_title' => __('Site settings', 'theme'),
//         'menu_slug' => 'site-settings',
//         'capability' => 'manage_options',
//         'redirect' => false,
//         'show_in_graphql' => true,
//         'graphql_field_name' => 'siteSettings'
//     ]);
//     if (function_exists('pll_languages_list')) {
//         $default_language = pll_default_language();
//         acf_add_options_page(array(
//             // 'page_title'     => 'Theme Settings '  . strtoupper($default_language),
//             // 'menu_title'    => 'Theme Settings '  . strtoupper($default_language),
//             'page_title'     => 'Theme Settings ' . strtoupper($default_language),
//             'menu_title'    => 'Theme Settings ' . strtoupper($default_language),
//             'menu_slug'     => 'theme-settings-'. strtoupper($default_language),
//             'capability'    => 'edit_posts',
//             'post_id' => 'theme_settings_' . $default_language,
//             'redirect'        => false,
//             'show_in_graphql' => true,
//         ));
//         $languages = pll_languages_list();
//         $non_default_languages = array_diff($languages, [$default_language]);
//         foreach ($non_default_languages as $a_language) {
//             acf_add_options_sub_page(array(
//                 'page_title'     => 'Theme Settings ' . strtoupper($a_language),
//                 'menu_title'    => 'Theme Settings ' . strtoupper($a_language),
//                 'menu_slug'    => 'theme-settings-' . strtoupper($a_language),
//                 'post_id' => 'theme_settings_' . $a_language,
//                 'parent_slug'     => 'theme-settings-' . strtoupper($default_language),
//                 'show_in_graphql' => true,
//             ));
//         }
//     }
// } else {
//     acf_add_options_page(array(
//         'page_title'     => 'Theme Settings',
//         'menu_title'    => 'Theme Settings',
//         'menu_slug'     => 'theme-settings',
//         'capability'    => 'edit_posts',
//         'redirect'        => false,
//         'show_in_graphql' => true,
//     ));
// }

function bfx_force_cross_type_unique_slugs( $slug, $post_ID, $post_status, $post_type, $post_parent ){
    
    global $wpdb, $wp_rewrite;
    
    //Don't touch hierarchical post types
    $hierarchical_post_types = get_post_types( array('hierarchical' => true) );
    if( in_array( $post_type, $hierarchical_post_types ) )
    return $slug;
    
    if( 'attachment' == $post_type){
        //These will be unique anyway
        return $slug;
    }
    
    
    $feeds = $wp_rewrite->feeds;
    if ( ! is_array( $feeds ) )
    $feeds = array();
    $excluded_post_types = ['nav_menu_item'];
    foreach($excluded_post_types as $key => $a_cpt){
        $excluded_post_types[$key] = '"' . $excluded_post_types[$key] . '"';
    }
    $excluded_post_types_string = implode(",", $excluded_post_types);
    
    //Lets make sure the slug is really unique:
    $check_sql = "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND ID != %d AND post_type NOT IN ($excluded_post_types_string) LIMIT 1";
    $post_name_check = $wpdb->get_var( $wpdb->prepare( $check_sql, $slug, $post_ID ) );
    
    if ( $post_name_check || in_array( $slug, $feeds) ) {
        $suffix = 2;
        
        do {
            $alt_post_name = substr ($slug, 0, 200 - ( strlen( $suffix ) + 1 ) ) . "-$suffix";
            $post_name_check = $wpdb->get_var( $wpdb->prepare($check_sql, $alt_post_name, $post_ID ) );
            $suffix++;
        } while ( $post_name_check );
        
        $slug = $alt_post_name;
    }
    
    return $slug;
}
add_filter('wp_unique_post_slug', 'bfx_force_cross_type_unique_slugs',10,5);

// WP_CLI::add_command("bfx_change_acf_field_name", function(){
//     $test = 0;
//     echo "Ok Boomer";
// });